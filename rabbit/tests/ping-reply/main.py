#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-3.0-or-later

"""This program is a test for network virtual appliance "rabbit".

The test checks that rabbit responds to pings.

.. code:: text

   +----------------+
   |                |
   | rabbit         |
   |                |
   | 192.168.0.1/24 |
   |                |
   +-------+--------+
           |
           |
           |
   +-------+--------+
   |                |
   | 192.168.0.2/24 |
   |                |
   | node0          |
   |                |
   +----------------+

Steps
-----

1. Configure rabbit network interface.
2. Configure node 0.
3. Ping rabbit from node 0.
4. If ping is successfull, test passes.
"""

import logging

import alpy.container
import alpy.utils

import carrot


def main():
    config = carrot.collect_config(link_count=1)

    alpy.utils.configure_logging()
    logger = logging.getLogger(__name__)
    logger.info(
        "Test description: Configure rabbit network interface and check "
        "connectivity by pinging it from node 0."
    )

    with carrot.run(config) as resources:
        with carrot.user_session(resources.console, resources.qmp):
            carrot.configure(resources.console, config.timeout)
            configure_node(resources.docker_client, config.timeout)
            ping_rabbit(resources.docker_client, config.timeout)


def configure_node(docker_client, timeout):
    alpy.container.add_ip_address(
        "node0",
        "192.168.0.2/24",
        docker_client=docker_client,
        image=carrot.IMAGE_BUSYBOX,
        timeout=timeout,
    )


def ping_rabbit(docker_client, timeout):
    logger = logging.getLogger(__name__)
    context_logger = alpy.utils.make_context_logger(logger)
    with context_logger("Ping rabbit"):
        container = docker_client.containers.create(
            carrot.IMAGE_BUSYBOX,
            ["ping", "-c", "1", "192.168.0.1"],
            network_mode="container:node0",
        )
        try:
            container.start()
            alpy.container.close(container, timeout)
        finally:
            container.remove()


if __name__ == "__main__":
    main()
